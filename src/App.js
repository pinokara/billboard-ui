import './App.css';
import { WalletContextProvider } from './common/WalletContext';
import Home from './Home';

function App() {
  return (
    <WalletContextProvider>
      <Home />
    </WalletContextProvider>
  );
}

export default App;
