import React, { useState, useRef, useEffect } from 'react';
import { Button, Modal, Input, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useConnect, useGetDataWallet, useDisconnect } from './common/WalletContext/hooks';
import Status from './Status';
import useMethodSend from './common/WalletContext/hooks/useMethodSend';
import { useWeb3 } from './common/WalletContext/hooks/useWeb3';
import { BILLBOARD_NAME } from './contracts';
import { BILLBOARD_ADDRESS } from './constants/network';

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function ButtonGroup() {
  const { address, connected } = useGetDataWallet();
  const [openModalMessage, setOpenModalMessage] = useState(false);
  const web3 = useWeb3();
  const disconnect = useDisconnect();
  const [messageInput, setMessageInput] = useState('');
  const [setMessage, { status }] = useMethodSend(BILLBOARD_NAME, 'setMessage');
  const prevStatus = usePrevious(status);
  useEffect(() => {
    if (status === 'success' && prevStatus === 'pending') {
      setOpenModalMessage(false);
    }
  }, [status])
  const connectWallet = useConnect();
  return (
    <div className="button-group">
      {!connected ? <Button onClick={connectWallet}>Connect</Button> : (
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
          <Modal
            title="Set your message"
            visible={openModalMessage}
            onOk={() => setMessage([web3.utils.fromAscii(messageInput)], { from: address })}
            onCancel={() => setOpenModalMessage(false)}
          >
            <Spin spinning={status === 'pending'} indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}>
              <Input placeholder="Your message" onChange={(e) => setMessageInput(e.target.value)}/>
            </Spin>
          </Modal>
          <Status />
          <Button style={{ marginBottom: 10 }} onClick={() => setOpenModalMessage(true)}>Set Billboad Message</Button>
          <Button style={{ marginBottom: 10 }} onClick={disconnect}>Disconnect</Button>
        </div>
      )}
    </div>
  );
}

export default ButtonGroup;
