import './App.css';
import { Row, Col, Card } from 'antd';
import ButtonGroup from './ButtonGroup';
import { BILLBOARD_NAME } from './contracts';
import { BILLBOARD_ADDRESS } from './constants/network';
import { useMethodCallOnMount } from './common/WalletContext/hooks/useMethodCall';
import { useWeb3 } from './common/WalletContext/hooks/useWeb3';

function App() {
  const web3 = useWeb3();
  const message = useMethodCallOnMount(
    BILLBOARD_NAME,
    'message',
  )

  return (
    <div className="App">
      <Row gutter={16}>
        <Col span={24}>
        <Card title="Message box" bordered={true} style={{
            marginBottom: '10px'
        }}>
          {web3 && message && web3.utils.toUtf8(message)}
        </Card>
        <ButtonGroup />
        </Col>
      </Row>
    </div>
  );
}

export default App;
