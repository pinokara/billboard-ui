import React, { useState, useRef, useEffect } from 'react';
import { Button, Statistic, Row, Col, InputNumber, Modal, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useGetDataWallet } from './common/WalletContext/hooks';
import { useMethodCallOnMount } from './common/WalletContext/hooks/useMethodCall';
import useMethodSend from './common/WalletContext/hooks/useMethodSend';
import { STABLY_TOKEN_NAME } from './contracts';
import { BILLBOARD_ADDRESS } from './constants/network';

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function Status() {
  const { address, connected } = useGetDataWallet();
  const [approveAmount, setApprove] = useState(100);
  const [openApproveModal, setOpenApproveModal] = useState(false);
  const [approve, { status }] = useMethodSend(STABLY_TOKEN_NAME, 'approve');

  const prevStatus = usePrevious(status);
  useEffect(() => {
    if (status === 'success' && prevStatus === 'pending') {
      setOpenApproveModal(false);
    }
  }, [status])

  const allowance = useMethodCallOnMount(
    STABLY_TOKEN_NAME,
    'allowance',
    [address, BILLBOARD_ADDRESS]
  )

  const amount = useMethodCallOnMount(
    STABLY_TOKEN_NAME,
    'balanceOf',
    [address]
  )

  const onChange = (value) => {
    setApprove(value)
  }
  
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    }}>
      <Modal
        title="Approve Stably"
        visible={openApproveModal}
        onOk={() => approve([BILLBOARD_ADDRESS, approveAmount * 1000000], { from: address })}
        onCancel={() => setOpenApproveModal(false)}
      >
        <Spin spinning={status === 'pending'} indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}>
          <InputNumber
            min={1}
            max={Number(amount)/1000000}
            onChange={onChange}
            defaultValue={100}
            value={approveAmount}
            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
            size='large'
          />
        </Spin>
      </Modal>
      <Statistic title="Address" value={formatAddress(address)} />
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
      }}>
        <Row gutter={20}>
          <Col span={12}>
            <Statistic title="Amount" value={Number(amount)/1000000} />
          </Col>
          <Col span={12}>
            <Statistic title="Allowance" value={Number(allowance)/1000000} />
          </Col>
        </Row>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
      }}>
        <Button style={{ marginBottom: 10 }} onClick={() => setOpenApproveModal(true)}>Approve Stably</Button>
      </div>
    </div>
  );
}

export default Status;

const formatAddress = (address) => {
  return [
    address[0],
    address[1],
    address[2],
    address[3],
    '.',
    '.',
    '.',
    address[address.length - 4],
    address[address.length - 3],
    address[address.length - 2],
    address[address.length - 1],
  ].join('')
}