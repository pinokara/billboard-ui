export const TRANSACTION_STATUS = {
  pending: 'pending',
  success: 'success',
  error: 'error',
};
