import { useContext } from 'react';
import WalletContext from '..';

export const useConnect = () => {
  const { onConnect } = useContext(WalletContext);
  return onConnect;
};

export const useDisconnect = () => {
  const { resetWallet } = useContext(WalletContext);
  return resetWallet;
};

export const useGetDataWallet = () => {
  const { walletInfo } = useContext(WalletContext);
  return walletInfo;
};

export const useAccount = () => {
  const { walletInfo } = useContext(WalletContext);
  return walletInfo?.address;
};

export const useWrongNetwork = () => {
  const { walletInfo } = useContext(WalletContext);
  return walletInfo?.isWrongNetwork;
};

export const useContractInfo = (contractName) => {
  const { contractInfos } = useContext(WalletContext);
  return (contractInfos || {})[contractName];
};

export const useAllContracts = (contractName) => {
  const { contractInfos } = useContext(WalletContext);
  return contractInfos || {};
};
