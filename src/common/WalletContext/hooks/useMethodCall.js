import { useCallback, useEffect, useState } from 'react';
import { useContractInfo } from '.';

const useMethodCall = (contractName, methodName) => {
  const [data, setData] = useState(null);

  const contract = useContractInfo(contractName);

  const triggerCall = useCallback(
    (args, opts) => {
      if (!contract) {
        console.log(`[CallMethod] Waiting to init contract ${contractName}`);
        return;
      }
      const callMethod = contract.methods[methodName];
      if (!callMethod) {
        console.error(
          `[CallMethod] No method ${methodName} in contract ${contractName}`
        );
        return;
      }
      callMethod(...args)
        .call(opts)
        .then((result) => {
          setData(result);
        });
    },
    [contract, contractName, methodName]
  );

  return [triggerCall, data];
};

export const useMethodCallOnMount = (
  contractName,
  methodName,
  args = [],
  opts = {}
) => {
  const [triggerCall, data] = useMethodCall(contractName, methodName);

  useEffect(() => {
    triggerCall(args, opts);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(args), JSON.stringify(opts), triggerCall]);

  return data;
};

export default useMethodCall;
