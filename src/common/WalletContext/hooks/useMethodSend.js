import { useCallback, useState } from 'react';

import { useContractInfo, useAccount, useConnect } from '.';
import { TRANSACTION_STATUS } from '../constants';

const useMethodSend = (contractName, methodName) => {
  const [receipt, setReceipt] = useState();
  const [isSigning, setSigningState] = useState(false);
  const [status, setStatus] = useState();
  const contract = useContractInfo(contractName);
  const address = useAccount();
  const connectWallet = useConnect();

  const triggerSend = useCallback(
    (args, opts) => {
      if (!address) {
        connectWallet();
        return;
      }
      if (!contract) {
        console.warn(`[SendMethod] Waiting to init contract ${contractName}`);
        return;
      }
      const sendMethod = contract.methods[methodName];
      if (!sendMethod) {
        console.error(
          `[SendMethod] No method ${methodName} in contract ${contractName} found`
        );
        return;
      }
      setSigningState(true);
      setStatus();
      setReceipt();
      sendMethod(...args)
        .send(opts)
        .on('transactionHash', (hash) => {
          setStatus(TRANSACTION_STATUS.pending);
          setSigningState(false);
        })
        .on('receipt', (receipt) => {
          setStatus(TRANSACTION_STATUS.success);
          setReceipt(receipt);
        })
        // .on('confirmation', (confirmationNumber, receipt) => {
        //   console.log('confirmation receipt', receipt);
        //   console.log('confirmationNumber', confirmationNumber);
        //   setStatus(TRANSACTION_STATUS.success);
        //   setReceipt({ confirmationNumber, receipt });
        // })
        .on('error', (error) => {
          setStatus(TRANSACTION_STATUS.error);
          setSigningState(false);
        });
    },
    [address, contract, methodName, connectWallet, contractName]
  );

  return [triggerSend, { status, receipt }, isSigning];
};

export default useMethodSend;
