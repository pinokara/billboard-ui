import { useContext } from 'react';
import WalletContext from '..';

export const useWeb3 = () => {
  const { walletInfo } = useContext(WalletContext);
  return walletInfo?.web3;
};
