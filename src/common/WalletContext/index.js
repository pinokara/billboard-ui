import React, { createContext, useCallback, useState } from 'react';
import Web3 from 'web3';
import Web3Modal from 'web3modal';
import { getProviderOptions } from './utils';
import { useEffect } from 'react';
import { CONTRACT_INFOS } from '../../contracts';
import { NETWORK, NETWORK_ID, CHAIN_ID } from '../../constants/network';
import useDisableEthereumReload from './hooks/useDisableEthereumReload';

const WalletContext = createContext({});

const INITIAL_DATA_STATE = {
  address: null,
  web3: null,
  provider: null,
  connected: false,
  chainId: CHAIN_ID,
  networkId: NETWORK_ID,
  isWrongNetwork: false,
};

// Init contract
const initContractInfos = (web3) => {
  const contracts = {};
  CONTRACT_INFOS.map((info) => {
    const { abi, address, name } = info;
    contracts[name] = new web3.eth.Contract(abi, address);
    return info;
  });
  return contracts;
};

// Init web3
const initWeb3 = (provider) => {
  const web3 = new Web3(provider);

  web3.eth.extend({
    methods: [
      {
        name: 'chainId',
        call: 'eth_chainId',
        outputFormatter: web3.utils.hexToNumber,
      },
    ],
  });

  const contracts = initContractInfos(web3);

  return {
    web3,
    contracts,
  };
};

export const WalletContextProvider = ({ children }) => {
  useDisableEthereumReload();
  const [walletInfo, setWalletInfo] = useState(INITIAL_DATA_STATE);
  const [contractInfos, setContractInfos] = useState({});

  const providerOptions = getProviderOptions();
  const web3Modal = new Web3Modal({
    network: NETWORK,
    cacheProvider: true,
    providerOptions,
  });

  const resetWallet = useCallback(async () => {
    const { web3 } = walletInfo;
    if (web3 && web3.currentProvider && web3.currentProvider.close) {
      await web3.currentProvider.close();
    }
    await web3Modal.clearCachedProvider();
    await web3Modal.resetState();
    await setWalletInfo(INITIAL_DATA_STATE);
  }, [walletInfo, web3Modal]);

  const subscribeProvider = useCallback(
    async (provider) => {
      if (!provider.on) {
        return;
      }
      provider.on('close', async () => {
        await resetWallet();
      });
      provider.on('accountsChanged', async (accounts) => {
        window.location.reload();
      });
      provider.on('chainChanged', async (chainId) => {
        window.location.reload();
      });

      provider.on('networkChanged', async (networkId) => {
        window.location.reload();
      });

      provider.on('disconnect', (error) => {
        console.log('====disconnect error', error);
      });
    },
    [resetWallet]
  );

  const onConnect = useCallback(async () => {
    try {
      const provider = await web3Modal.connect();
      await subscribeProvider(provider);

      const { web3, contracts } = initWeb3(provider);
      const accounts =  await web3.eth.getAccounts();
      const networkId = await web3.eth.net.getId();
      const isWrongNetwork = networkId && networkId !== NETWORK_ID;
      const chainId = await web3.eth.chainId();
      setContractInfos(contracts);
      setWalletInfo({
        web3,
        provider,
        connected: true,
        address: accounts[0],
        chainId,
        networkId,
        isWrongNetwork,
      });
    } catch (error) {
      await resetWallet();
    }
  }, [resetWallet, subscribeProvider, web3Modal]);

  useEffect(() => {
    if (localStorage.getItem('walletconnect') !== null || (window.ethereum && window.ethereum.isConnected())) {
      onConnect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <WalletContext.Provider
      value={{
        resetWallet,
        onConnect,
        web3Modal,
        walletInfo,
        contractInfos,
        setContractInfos,
      }}
    >
      {children}
    </WalletContext.Provider>
  );
};

export default WalletContext;
