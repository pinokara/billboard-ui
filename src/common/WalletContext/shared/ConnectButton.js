import React from 'react';

import { Button } from '@blank/atoms';
import { useConnect } from '@blank/common/WalletContext/hooks';

const ConnectButton = (props) => {
  const connectWallet = useConnect();

  return (
    <Button {...props} onClick={connectWallet}>
      Connect
    </Button>
  );
};

export default ConnectButton;
