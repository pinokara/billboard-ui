import WalletConnectProvider from '@walletconnect/web3-provider';
import Fortmatic from 'fortmatic';
import Portis from '@portis/web3';
import { FORTMATIC_KEY, PORTIS_KEY, INFURA_ID } from '../../../constants/wallets';

export const getProviderOptions = () => {
  const providerOptions = {
    injected: {
      package: null,
    },
    walletconnect: {
      package: WalletConnectProvider,
      options: {
        infuraId: INFURA_ID,
      },
    },
    fortmatic: {
      package: Fortmatic,
      options: {
        key: FORTMATIC_KEY,
      },
    },
    portis: {
      package: Portis,
      options: {
        id: PORTIS_KEY,
      },
    },
  };
  return providerOptions;
};
