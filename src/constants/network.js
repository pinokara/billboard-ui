import * as contractAddress from './contract-address';

export const MAINNET = 'main';
export const RINKEBY = 'rinkeby';
export const IS_NETWORK_RINKEBY = process.env.REACT_APP_NETWORK === RINKEBY; 

const networkConfig = {
  [MAINNET]: {
    id: 1,
    chainId: 1,
    name: 'Mainnet',
    network: 'mainnet',
    domain: 'blanknetwork.io',
    etherscan: 'etherscan.io',
    billboardAddress: '',
    stablyTokenAddress: '',
  },
  [RINKEBY]: {
    id: 4,
    chainId: 4,
    name: 'Rinkeby Testnet',
    network: 'rinkeby',
    domain: 'rinkeby.blanknetwork.io',
    etherscan: 'rinkeby.etherscan.io',
    billboardAddress: contractAddress.BILLBOARD_ADDRESS,
    stablyTokenAddress: contractAddress.STABLY_TOKEN_ADDRESS,
  },
};

export const {
  id: NETWORK_ID,
  chainId: CHAIN_ID,
  name: NETWORK_NAME,
  domain: DOMAIN,
  network: NETWORK,
  etherscan: ETHERSCAN_DOMAIN,
  billboardAddress: BILLBOARD_ADDRESS,
  stablyTokenAddress: STABLY_TOKEN_ADDRESS,
} = networkConfig[process.env.REACT_APP_NETWORK || RINKEBY];
