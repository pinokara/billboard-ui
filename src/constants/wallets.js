import { MAINNET, RINKEBY } from './network';

export const METAMASK = 'metamask';
export const PORTIS = 'portis';
export const WALLET_CONNECT = 'walletconnect';
export const FORTMATIC = 'fortmatic';

const walletConfig = {
  [MAINNET]: {
    INFURA_ID: 'e8a57f941efb4963859baf57ee3ac209',
    INFURA_ENDPOINT:
      'https://mainnet.infura.io/v3/e8a57f941efb4963859baf57ee3ac209',
    FORTMATIC_KEY: '',
    PORTIS_KEY: '',
  },
  [RINKEBY]: {
    INFURA_ID: 'e8a57f941efb4963859baf57ee3ac209',
    INFURA_ENDPOINT:
      'https://rinkeby.infura.io/v3/e8a57f941efb4963859baf57ee3ac209',
    FORTMATIC_KEY: '',
    PORTIS_KEY: '',
  },
};

export const {
  FORTMATIC_KEY,
  PORTIS_KEY,
  INFURA_ID,
  INFURA_ENDPOINT,
} = walletConfig[process.env.REACT_APP_NETWORK || RINKEBY];
