import BillBoard from './BillBoard.json';
import StablyToken from './StablyToken.json';
import {
  BILLBOARD_ADDRESS,
  STABLY_TOKEN_ADDRESS,
} from '../constants/contract-address';

export const BILLBOARD_NAME = 'Billboard';
export const STABLY_TOKEN_NAME = 'StablyToken';

export const CONTRACT_INFOS = [
  {
    name: BILLBOARD_NAME,
    abi: BillBoard.abi,
    address: BILLBOARD_ADDRESS,
  },
  {
    name: STABLY_TOKEN_NAME,
    abi: StablyToken.abi,
    address: STABLY_TOKEN_ADDRESS,
  },
];
